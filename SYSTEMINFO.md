# IpFabric

Vendor: IP Fabric
Homepage: https://ipfabric.io/

Product: IP Fabric
Product Page: https://ipfabric.io/

## Introduction
We classify IP Fabric into the Service Assurance domain as IP Fabric provides continuous validation of your multi-vendor, multi-domain network state.

## Why Integrate
The IP Fabricadapter from Itential is used to integrate the Itential Automation Platform (IAP) with IP Fabric. With this adapter you have the ability to perform operations with IP Fabric on items such as:

- Networks
- Inventory
- Interfaces
- Neighbors
- QoS
- Routing
- Platforms

## Additional Product Documentation
The [IP Fabric API](https://www.postman.com/ipfabric/workspace/ip-fabric-public-workspace/overview)
