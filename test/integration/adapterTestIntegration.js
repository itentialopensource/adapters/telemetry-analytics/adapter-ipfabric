/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-ipfabric',
      type: 'IpFabric',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const IpFabric = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] IpFabric Adapter Test', () => {
  describe('IpFabric Class Tests', () => {
    const a = new IpFabric(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const osPostOsCleanDbBodyParam = {
      keepSettings: true
    };
    describe('#postOsCleanDb - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOsCleanDb(osPostOsCleanDbBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Os', 'postOsCleanDb', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const osPostOsGenerateNimpeeCertBodyParam = {
      city: 'Prague',
      country: 'CZ',
      dnsShort: true,
      organization: 'network.net',
      state: 'Czech'
    };
    describe('#postOsGenerateNimpeeCert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOsGenerateNimpeeCert(osPostOsGenerateNimpeeCertBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Os', 'postOsGenerateNimpeeCert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOsMaintenance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOsMaintenance((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Os', 'postOsMaintenance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const osPostOsTechsupportBodyParam = {
      discoveryServicesLogs: false,
      snapshot: {},
      systemLogs: true
    };
    describe('#postOsTechsupport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postOsTechsupport(osPostOsTechsupportBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Os', 'postOsTechsupport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOsUploadNimpeeCert - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOsUploadNimpeeCert(null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Os', 'postOsUploadNimpeeCert', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOsHostname - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOsHostname((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('demo4.ipfabric.io', data.response.hostname);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Os', 'getOsHostname', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const osServiceId = 'fakedata';
    const osSnapshotId = 'fakedata';
    describe('#getOsServiceLogServiceIdSnapshotId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOsServiceLogServiceIdSnapshotId(osServiceId, osSnapshotId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Os', 'getOsServiceLogServiceIdSnapshotId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOsVersion - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOsVersion((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('4.0.0', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Os', 'getOsVersion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSupportStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSupportStatus((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.arango);
                assert.equal('object', typeof data.response.cpu);
                assert.equal(8, data.response.cpuCount);
                assert.equal('object', typeof data.response.disk);
                assert.equal('object', typeof data.response.memory);
                assert.equal(true, data.response.mongo);
                assert.equal(true, data.response.rabbit);
                assert.equal(true, data.response.syslogWorker);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Os', 'getSupportStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobsJobId = 'fakedata';
    describe('#postJobsJobIdCancel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postJobsJobIdCancel(jobsJobId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'postJobsJobIdCancel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postJobsJobIdStop - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postJobsJobIdStop(jobsJobId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'postJobsJobIdStop', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobsPostTablesJobsBodyParam = {
      columns: [
        'id',
        'downloadFile',
        'finishedAt',
        'isDone',
        'name',
        'scheduledAt',
        'snapshot',
        'startedAt',
        'status',
        'username'
      ]
    };
    describe('#postTablesJobs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesJobs(jobsPostTablesJobsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'postTablesJobs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getJobsJobIdDownload - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getJobsJobIdDownload(jobsJobId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'getJobsJobIdDownload', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersPostTablesUsersBodyParam = {
      columns: [
        'id',
        'customScope',
        'domainSuffixes',
        'email',
        'isLocal',
        'ldapId',
        'scope',
        'username'
      ]
    };
    describe('#postTablesUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesUsers(usersPostTablesUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'postTablesUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let usersUserId = 'fakedata';
    const usersPostUsersBodyParam = {
      username: 'ipf_user',
      password: samProps.authentication.password,
      email: 'user@ipfabric.io',
      scope: [
        'read',
        'settings',
        'team',
        'write'
      ]
    };
    describe('#postUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsers(usersPostUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.customScope);
                assert.equal('user@ipfabric.io', data.response.email);
                assert.equal(true, Array.isArray(data.response.scope));
                assert.equal('ipf_user', data.response.username);
                assert.equal(true, data.response.active);
                assert.equal('...', data.response.ldap);
                assert.equal('905232354', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              usersUserId = data.response.id;
              saveMockData('Users', 'postUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsersMePassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsersMePassword((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.customScope);
                assert.equal('user@ipfabric.io', data.response.email);
                assert.equal(true, Array.isArray(data.response.scope));
                assert.equal('ipf_user', data.response.username);
                assert.equal(true, data.response.active);
                assert.equal('...', data.response.ldap);
                assert.equal('905232354', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'postUsersMePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersPostUsersUserIdPasswordBodyParam = {
      oldPassword: 'someOldPassword',
      newPassword: samProps.authentication.password
    };
    describe('#postUsersUserIdPassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postUsersUserIdPassword(usersUserId, usersPostUsersUserIdPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.customScope);
                assert.equal('user@ipfabric.io', data.response.email);
                assert.equal(true, Array.isArray(data.response.scope));
                assert.equal('ipf_user', data.response.username);
                assert.equal(true, data.response.active);
                assert.equal('...', data.response.ldap);
                assert.equal('905232354', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'postUsersUserIdPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsers((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersMe - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchUsersMe((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'patchUsersMe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersMe - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersMe((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.customScope);
                assert.equal('user@ipfabric.io', data.response.email);
                assert.equal(true, Array.isArray(data.response.scope));
                assert.equal('ipf_user', data.response.username);
                assert.equal(true, data.response.active);
                assert.equal('...', data.response.ldap);
                assert.equal('905232354', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersMe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersMeSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersMeSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.diagrams);
                assert.equal('object', typeof data.response.tables);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersMeSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#patchUsersUserId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.patchUsersUserId(usersUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'patchUsersUserId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUserId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersUserId(usersUserId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.customScope);
                assert.equal('user@ipfabric.io', data.response.email);
                assert.equal(true, Array.isArray(data.response.scope));
                assert.equal('ipf_user', data.response.username);
                assert.equal(true, data.response.active);
                assert.equal('...', data.response.ldap);
                assert.equal('905232354', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersUserId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationPostAuthLoginBodyParam = {
      username: 'asdasd',
      password: samProps.authentication.password
    };
    describe('#postAuthLogin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAuthLogin(authenticationPostAuthLoginBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...', data.response.accessToken);
                assert.equal('eGdOyqN-VUPhSvnQv54BHQaA9vcr_UliFYGi...', data.response.refreshToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'postAuthLogin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthLogout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAuthLogout((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'postAuthLogout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationPostAuthTokenBodyParam = {
      refreshToken: 'oigBFo-WhSO0_P5PhcUrPyGeLERNRVPYWvwAQ...'
    };
    describe('#postAuthToken - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postAuthToken(authenticationPostAuthTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9...', data.response.accessToken);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'postAuthToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotsCreateSnapshotBodyParam = {
      allowTelnet: true,
      backup: {
        cron: '0 23 * * *',
        enabled: false,
        server: 'backup.server.io',
        type: 'ftp',
        username: 'username',
        password: samProps.authentication.password
      },
      checkSavedConfig: true,
      cliRetryLimit: {
        authFail: 2,
        default: 3,
        tacacs: {
          delay: 1000,
          retry: 1
        }
      },
      cliSessionsLimit: {
        enabled: false,
        limit: 5
      },
      credentials: [
        {
          excludeNetworks: [
            '1.1.1.1/32',
            '8.8.8.8/32'
          ],
          network: [
            '192.168.0.0/16',
            '10.0.0.0/8'
          ],
          expirationDate: {
            enabled: true,
            value: {}
          },
          id: '9a5340e5-ef75-4962-a694-fb7a683f8e2a',
          priority: 1,
          password: samProps.authentication.password,
          syslog: true
        },
        {
          excludeNetworks: [],
          network: [],
          expirationDate: {
            enabled: false
          },
          id: 'sdf340e5-ef75-4962-a694-fb7a6asd8e2a',
          priority: 2,
          syslog: true
        }
      ],
      privileges: [
        {
          excludeNetworks: [],
          expirationDate: {
            enabled: false
          },
          id: '3233fd2a-3036-4dd5-9845-e984e9238c00',
          includeNetworks: [
            '0.0.0.0/0'
          ],
          notes: 'Main Enable password',
          password: samProps.authentication.password,
          priority: 1,
          username: ''
        }
      ],
      customerName: 'Test Customer',
      fullBgpLimit: {
        enabled: true,
        threshold: 10000
      },
      limitDiscoveryTasks: {
        alreadyDiscovered: false,
        sourceOfTasks: [
          'xdp',
          'arp',
          'routes',
          'trace'
        ]
      },
      maintenance: {
        cron: '0 0 * * *',
        enabled: true
      },
      networks: {
        exclude: [
          '8.8.8.8/32'
        ],
        include: [
          '0.0.0.0/0'
        ],
        properties: {
          exclude: {
            type: 'array',
            items: {
              type: 'string',
              format: 'ipv4'
            }
          },
          include: {
            type: 'array',
            items: {
              type: 'string',
              format: 'ipv4'
            }
          }
        }
      },
      resolveNames: {
        discoveryDevices: true
      },
      scanner: {
        enabled: false,
        shortestMask: 20
      },
      seedList: [
        '192.168.110.3',
        '10.47.255.101'
      ],
      siteSeparation: [
        {
          id: '90b7dd11-1ab2-42c9-8665-0b137afe50a0',
          note: 'L1DC',
          regex: 'L1(?:R1[4-8]|R7|FW56|SW[3-4])',
          siteName: 'L1DC',
          transformation: 'uppercase',
          type: 'regex'
        },
        {
          id: '8680156c-bfc6-4a76-92f9-d0f8f5a7c0e5',
          note: 'MAIN REGEX Rule',
          regex: '^(\\w\\d{1,3}|HWLAB|HUA)',
          transformation: 'uppercase',
          type: 'regex'
        }
      ],
      siteTypeCalc: 'rules',
      snapshots: {
        cron: '0 * * * *',
        enabled: false,
        maxLoaded: 3,
        maxLocked: 1
      },
      snmp: {
        communityString: 'community04',
        enabled: false,
        hostIp: '192.168.10.10',
        locality: 'Prague, DC',
        systemContact: 'Mr. Johnson',
        username: 'snmpUser',
        version: 'v2c'
      },
      syslog: {
        cron: '0 0 * * *',
        time: 60,
        trigger: 'syslogtrigger'
      },
      timeouts: {
        login: 60,
        session: 60
      },
      traceroute: {
        protocol: 'icmp',
        scope: [
          '10.0.0.0/8',
          '100.64.0.0/10',
          '169.254.0.0/16',
          '172.16.0.0/12',
          '192.168.0.0/16'
        ]
      }
    };
    describe('#createSnapshot - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createSnapshot(snapshotsCreateSnapshotBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'createSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotsSnapshotId = 'fakedata';
    describe('#cloneSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cloneSnapshot(snapshotsSnapshotId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'cloneSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loadSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.loadSnapshot(snapshotsSnapshotId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'loadSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lockSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.lockSnapshot(snapshotsSnapshotId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'lockSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unloadSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unloadSnapshot(snapshotsSnapshotId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'unloadSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unlockSnapshots - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.unlockSnapshots(snapshotsSnapshotId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'unlockSnapshots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSnapshots - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSnapshots((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'getSnapshots', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotsRenameSnapshotBodyParam = {
      name: 'DC Migration - 27 Oct',
      note: 'Snapshot created after migration process'
    };
    describe('#renameSnapshot - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.renameSnapshot(snapshotsSnapshotId, snapshotsRenameSnapshotBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'renameSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.downloadSnapshot(snapshotsSnapshotId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'downloadSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snapshotsUpdateSnapshotSettingsBodyParam = {
      allowTelnet: true,
      backup: {
        cron: '0 23 * * *',
        enabled: false,
        server: 'backup.server.io',
        type: 'ftp',
        username: 'username',
        password: samProps.authentication.password
      },
      checkSavedConfig: true,
      cliRetryLimit: {
        authFail: 2,
        default: 3,
        tacacs: {
          delay: 1000,
          retry: 1
        }
      },
      cliSessionsLimit: {
        enabled: false,
        limit: 5
      },
      credentials: [
        {
          excludeNetworks: [
            '1.1.1.1/32',
            '8.8.8.8/32'
          ],
          network: [
            '192.168.0.0/16',
            '10.0.0.0/8'
          ],
          expirationDate: {
            enabled: true,
            value: {}
          },
          id: '9a5340e5-ef75-4962-a694-fb7a683f8e2a',
          priority: 1,
          password: samProps.authentication.password,
          syslog: true
        },
        {
          excludeNetworks: [],
          network: [],
          expirationDate: {
            enabled: false
          },
          id: 'sdf340e5-ef75-4962-a694-fb7a6asd8e2a',
          priority: 2,
          syslog: true
        }
      ],
      privileges: [
        {
          excludeNetworks: [],
          expirationDate: {
            enabled: false
          },
          id: '3233fd2a-3036-4dd5-9845-e984e9238c00',
          includeNetworks: [
            '0.0.0.0/0'
          ],
          notes: 'Main Enable password',
          password: samProps.authentication.password,
          priority: 1,
          username: ''
        }
      ],
      customerName: 'Test Customer',
      fullBgpLimit: {
        enabled: true,
        threshold: 10000
      },
      limitDiscoveryTasks: {
        alreadyDiscovered: false,
        sourceOfTasks: [
          'xdp',
          'arp',
          'routes',
          'trace'
        ]
      },
      maintenance: {
        cron: '0 0 * * *',
        enabled: true
      },
      networks: {
        exclude: [
          '8.8.8.8/32'
        ],
        include: [
          '0.0.0.0/0'
        ],
        properties: {
          exclude: {
            type: 'array',
            items: {
              type: 'string',
              format: 'ipv4'
            }
          },
          include: {
            type: 'array',
            items: {
              type: 'string',
              format: 'ipv4'
            }
          }
        }
      },
      resolveNames: {
        discoveryDevices: true
      },
      scanner: {
        enabled: false,
        shortestMask: 20
      },
      seedList: [
        '192.168.110.3',
        '10.47.255.101'
      ],
      siteSeparation: [
        {
          id: '90b7dd11-1ab2-42c9-8665-0b137afe50a0',
          note: 'L1DC',
          regex: 'L1(?:R1[4-8]|R7|FW56|SW[3-4])',
          siteName: 'L1DC',
          transformation: 'uppercase',
          type: 'regex'
        },
        {
          id: '8680156c-bfc6-4a76-92f9-d0f8f5a7c0e5',
          note: 'MAIN REGEX Rule',
          regex: '^(\\w\\d{1,3}|HWLAB|HUA)',
          transformation: 'uppercase',
          type: 'regex'
        }
      ],
      siteTypeCalc: 'rules',
      snapshots: {
        cron: '0 * * * *',
        enabled: false,
        maxLoaded: 3,
        maxLocked: 1
      },
      snmp: {
        communityString: 'community04',
        enabled: false,
        hostIp: '192.168.10.10',
        locality: 'Prague, DC',
        systemContact: 'Mr. Johnson',
        username: 'snmpUser',
        version: 'v2c'
      },
      syslog: {
        cron: '0 0 * * *',
        time: 60,
        trigger: 'syslogtrigger'
      },
      timeouts: {
        login: 60,
        session: 60
      },
      traceroute: {
        protocol: 'icmp',
        scope: [
          '10.0.0.0/8',
          '100.64.0.0/10',
          '169.254.0.0/16',
          '172.16.0.0/12',
          '192.168.0.0/16'
        ]
      }
    };
    describe('#updateSnapshotSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSnapshotSettings(snapshotsSnapshotId, snapshotsUpdateSnapshotSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'updateSnapshotSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readSnapshotSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.readSnapshotSettings(snapshotsSnapshotId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.allowTelnet);
                assert.equal('object', typeof data.response.backup);
                assert.equal(true, data.response.checkSavedConfig);
                assert.equal('object', typeof data.response.cliRetryLimit);
                assert.equal('object', typeof data.response.cliSessionsLimit);
                assert.equal(true, Array.isArray(data.response.credentials));
                assert.equal(true, Array.isArray(data.response.privileges));
                assert.equal('Test Customer', data.response.customerName);
                assert.equal('object', typeof data.response.fullBgpLimit);
                assert.equal('object', typeof data.response.limitDiscoveryTasks);
                assert.equal('object', typeof data.response.maintenance);
                assert.equal('object', typeof data.response.networks);
                assert.equal('object', typeof data.response.resolveNames);
                assert.equal('object', typeof data.response.scanner);
                assert.equal(true, Array.isArray(data.response.seedList));
                assert.equal(true, Array.isArray(data.response.siteSeparation));
                assert.equal('rules', data.response.siteTypeCalc);
                assert.equal('object', typeof data.response.snapshots);
                assert.equal('object', typeof data.response.snmp);
                assert.equal('object', typeof data.response.syslog);
                assert.equal('object', typeof data.response.timeouts);
                assert.equal('object', typeof data.response.traceroute);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'readSnapshotSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let settingsCredentialsId = 'fakedata';
    const settingsAddCredentialsBodyParam = {
      excludeNetworks: [
        '10.15.15.0/24'
      ],
      expirationDate: {
        enabled: true,
        value: '2025-10-23 23:59:59'
      },
      notes: 'The most beautiful password',
      password: samProps.authentication.password,
      syslog: false,
      username: 'someUserName',
      network: [
        '0.0.0.0/0'
      ]
    };
    describe('#addCredentials - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addCredentials(settingsAddCredentialsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.excludeNetworks));
                assert.equal('object', typeof data.response.expirationDate);
                assert.equal('The most beautiful password', data.response.notes);
                assert.equal(false, data.response.syslog);
                assert.equal(true, Array.isArray(data.response.network));
                assert.equal('string', data.response.id);
                assert.equal('someUserName', data.response.username);
                assert.equal('8093dc23e2', data.response.password);
              } else {
                runCommonAsserts(data, error);
              }
              settingsCredentialsId = data.response.id;
              saveMockData('Settings', 'addCredentials', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateJumphostSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateJumphostSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.allowTelnet);
                assert.equal('object', typeof data.response.backup);
                assert.equal(true, data.response.checkSavedConfig);
                assert.equal('object', typeof data.response.cliRetryLimit);
                assert.equal('object', typeof data.response.cliSessionsLimit);
                assert.equal(true, Array.isArray(data.response.credentials));
                assert.equal(true, Array.isArray(data.response.privileges));
                assert.equal('Test Customer', data.response.customerName);
                assert.equal('object', typeof data.response.fullBgpLimit);
                assert.equal('object', typeof data.response.limitDiscoveryTasks);
                assert.equal('object', typeof data.response.maintenance);
                assert.equal('object', typeof data.response.networks);
                assert.equal('object', typeof data.response.resolveNames);
                assert.equal('object', typeof data.response.scanner);
                assert.equal(true, Array.isArray(data.response.seedList));
                assert.equal(true, Array.isArray(data.response.siteSeparation));
                assert.equal('rules', data.response.siteTypeCalc);
                assert.equal('object', typeof data.response.snapshots);
                assert.equal('object', typeof data.response.snmp);
                assert.equal('object', typeof data.response.syslog);
                assert.equal('object', typeof data.response.timeouts);
                assert.equal('object', typeof data.response.traceroute);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'updateJumphostSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let settingsPrivilegesId = 'fakedata';
    const settingsAddEnablePasswordBodyParam = {
      excludeNetworks: [
        '10.15.15.0/24'
      ],
      expirationDate: {
        enabled: false,
        value: '2025-10-23T23:59:59.000Z'
      },
      includeNetworks: [
        '0.0.0.0/0'
      ],
      username: 'someUserName',
      password: samProps.authentication.password
    };
    describe('#addEnablePassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addEnablePassword(settingsAddEnablePasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.excludeNetworks));
                assert.equal('object', typeof data.response.expirationDate);
                assert.equal('string', data.response.id);
                assert.equal(true, Array.isArray(data.response.includeNetworks));
                assert.equal('Main Enable password', data.response.notes);
                assert.equal('8093dc23e2', data.response.password);
                assert.equal('someUserName', data.response.username);
                assert.equal(9, data.response.priority);
              } else {
                runCommonAsserts(data, error);
              }
              settingsPrivilegesId = data.response.id;
              saveMockData('Settings', 'addEnablePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsUpdateSeedSettingsBodyParam = [
      'string'
    ];
    describe('#updateSeedSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSeedSettings(settingsUpdateSeedSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.success);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'updateSeedSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let settingsRuleId = 'fakedata';
    const settingsUpdateSiteSeparationSettingsBodyParam = {
      note: 'L1DC',
      regex: 'L1(?:R1[4-8]|R7|FW56|SW[3-4])',
      siteName: 'L1DC',
      transformation: 'uppercase',
      type: 'regex',
      wan: [
        'serial',
        'tunel'
      ]
    };
    describe('#updateSiteSeparationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSiteSeparationSettings(settingsUpdateSiteSeparationSettingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response.id);
                assert.equal('L1DC', data.response.note);
                assert.equal('L1(?:R1[4-8]|R7|FW56|SW[3-4])', data.response.regex);
                assert.equal('L1DC', data.response.siteName);
                assert.equal('uppercase', data.response.transformation);
                assert.equal('regex', data.response.type);
                assert.equal(true, Array.isArray(data.response.wan));
              } else {
                runCommonAsserts(data, error);
              }
              settingsRuleId = data.response.id;
              saveMockData('Settings', 'updateSiteSeparationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readVendorApiSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.readVendorApiSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.allowTelnet);
                assert.equal('object', typeof data.response.backup);
                assert.equal(true, data.response.checkSavedConfig);
                assert.equal('object', typeof data.response.cliRetryLimit);
                assert.equal('object', typeof data.response.cliSessionsLimit);
                assert.equal(true, Array.isArray(data.response.credentials));
                assert.equal(true, Array.isArray(data.response.privileges));
                assert.equal('Test Customer', data.response.customerName);
                assert.equal('object', typeof data.response.fullBgpLimit);
                assert.equal('object', typeof data.response.limitDiscoveryTasks);
                assert.equal('object', typeof data.response.maintenance);
                assert.equal('object', typeof data.response.networks);
                assert.equal('object', typeof data.response.resolveNames);
                assert.equal('object', typeof data.response.scanner);
                assert.equal(true, Array.isArray(data.response.seedList));
                assert.equal(true, Array.isArray(data.response.siteSeparation));
                assert.equal('rules', data.response.siteTypeCalc);
                assert.equal('object', typeof data.response.snapshots);
                assert.equal('object', typeof data.response.snmp);
                assert.equal('object', typeof data.response.syslog);
                assert.equal('object', typeof data.response.timeouts);
                assert.equal('object', typeof data.response.traceroute);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'readVendorApiSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateGlobalSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateGlobalSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'updateGlobalSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readGlobalSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.readGlobalSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.allowTelnet);
                assert.equal('object', typeof data.response.backup);
                assert.equal(true, data.response.checkSavedConfig);
                assert.equal('object', typeof data.response.cliRetryLimit);
                assert.equal('object', typeof data.response.cliSessionsLimit);
                assert.equal(true, Array.isArray(data.response.credentials));
                assert.equal(true, Array.isArray(data.response.privileges));
                assert.equal('Test Customer', data.response.customerName);
                assert.equal('object', typeof data.response.fullBgpLimit);
                assert.equal('object', typeof data.response.limitDiscoveryTasks);
                assert.equal('object', typeof data.response.maintenance);
                assert.equal('object', typeof data.response.networks);
                assert.equal('object', typeof data.response.resolveNames);
                assert.equal('object', typeof data.response.scanner);
                assert.equal(true, Array.isArray(data.response.seedList));
                assert.equal(true, Array.isArray(data.response.siteSeparation));
                assert.equal('rules', data.response.siteTypeCalc);
                assert.equal('object', typeof data.response.snapshots);
                assert.equal('object', typeof data.response.snmp);
                assert.equal('object', typeof data.response.syslog);
                assert.equal('object', typeof data.response.timeouts);
                assert.equal('object', typeof data.response.traceroute);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'readGlobalSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readCredentials - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.readCredentials((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.data);
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'readCredentials', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCredentialSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateCredentialSettings(settingsCredentialsId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'updateCredentialSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readEnablePasswords - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.readEnablePasswords((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'readEnablePasswords', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateEnablePassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateEnablePassword(settingsPrivilegesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'updateEnablePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rewriteSeedSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rewriteSeedSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'rewriteSeedSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSeedSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSeedSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'getSeedSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#readSiteSeparationSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.readSiteSeparationSettings((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.data);
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'readSiteSeparationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateSiteSeparationRule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.updateSiteSeparationRule(settingsRuleId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'updateSiteSeparationRule', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simulatePath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.simulatePath(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Simulation', 'simulatePath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#simulatePath - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.simulatePath(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Simulation', 'simulatePath', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const pathLookupBodyParam = {
      parameters: {
        fake: 'data',
        enableRegions: 'data1'
      }
    };
    describe('#pathLookup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pathLookup(pathLookupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Simulation', 'pathLookup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pathLookupSvg - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pathLookupSvg(pathLookupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Simulation', 'pathLookupSvg', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pathLookupPng - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.pathLookupPng(pathLookupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Simulation', 'pathLookupPng', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastIgmpGroupsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastIgmpGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastIgmpGroups(multicastPostTablesMulticastIgmpGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastIgmpGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastIgmpInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastIgmpInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastIgmpInterfaces(multicastPostTablesMulticastIgmpInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastIgmpInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastIgmpSnoopingGlobalBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastIgmpSnoopingGlobal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastIgmpSnoopingGlobal(multicastPostTablesMulticastIgmpSnoopingGlobalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastIgmpSnoopingGlobal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastIgmpSnoopingGroupsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastIgmpSnoopingGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastIgmpSnoopingGroups(multicastPostTablesMulticastIgmpSnoopingGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastIgmpSnoopingGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastIgmpSnoopingVlansBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastIgmpSnoopingVlans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastIgmpSnoopingVlans(multicastPostTablesMulticastIgmpSnoopingVlansBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastIgmpSnoopingVlans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastMacBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastMac - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastMac(multicastPostTablesMulticastMacBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastMac', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastPimInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastPimInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastPimInterfaces(multicastPostTablesMulticastPimInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastPimInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastPimNeighborsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastPimNeighbors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastPimNeighbors(multicastPostTablesMulticastPimNeighborsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastPimNeighbors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastPimRpBsrBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastPimRpBsr - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastPimRpBsr(multicastPostTablesMulticastPimRpBsrBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastPimRpBsr', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastPimRpMappingsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastPimRpMappings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastPimRpMappings(multicastPostTablesMulticastPimRpMappingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastPimRpMappings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastPimRpMappingsGroupsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastPimRpMappingsGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastPimRpMappingsGroups(multicastPostTablesMulticastPimRpMappingsGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastPimRpMappingsGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastPimRpOverviewBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastPimRpOverview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastPimRpOverview(multicastPostTablesMulticastPimRpOverviewBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastPimRpOverview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastRoutesCountersBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastRoutesCounters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastRoutesCounters(multicastPostTablesMulticastRoutesCountersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastRoutesCounters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastRoutesFirstHopRouterBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastRoutesFirstHopRouter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastRoutesFirstHopRouter(multicastPostTablesMulticastRoutesFirstHopRouterBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastRoutesFirstHopRouter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastRoutesOutgoingInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastRoutesOutgoingInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastRoutesOutgoingInterfaces(multicastPostTablesMulticastRoutesOutgoingInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastRoutesOutgoingInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastRoutesOverviewBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastRoutesOverview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastRoutesOverview(multicastPostTablesMulticastRoutesOverviewBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastRoutesOverview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastRoutesSourcesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastRoutesSources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastRoutesSources(multicastPostTablesMulticastRoutesSourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastRoutesSources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastPostTablesMulticastRoutesTableBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesMulticastRoutesTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMulticastRoutesTable(multicastPostTablesMulticastRoutesTableBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'postTablesMulticastRoutesTable', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spanningTreePostTablesSpanningTreeBridgesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesSpanningTreeBridges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSpanningTreeBridges(spanningTreePostTablesSpanningTreeBridgesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpanningTree', 'postTablesSpanningTreeBridges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spanningTreePostTablesSpanningTreeGuardsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesSpanningTreeGuards - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSpanningTreeGuards(spanningTreePostTablesSpanningTreeGuardsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpanningTree', 'postTablesSpanningTreeGuards', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spanningTreePostTablesSpanningTreeInconsistenciesMultipleStpBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesSpanningTreeInconsistenciesMultipleStp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSpanningTreeInconsistenciesMultipleStp(spanningTreePostTablesSpanningTreeInconsistenciesMultipleStpBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpanningTree', 'postTablesSpanningTreeInconsistenciesMultipleStp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spanningTreePostTablesSpanningTreeInconsistenciesNeighborPortsVlanMismatchBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesSpanningTreeInconsistenciesNeighborPortsVlanMismatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSpanningTreeInconsistenciesNeighborPortsVlanMismatch(spanningTreePostTablesSpanningTreeInconsistenciesNeighborPortsVlanMismatchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpanningTree', 'postTablesSpanningTreeInconsistenciesNeighborPortsVlanMismatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spanningTreePostTablesSpanningTreeInconsistenciesPortsMultipleNeighborsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesSpanningTreeInconsistenciesPortsMultipleNeighbors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSpanningTreeInconsistenciesPortsMultipleNeighbors(spanningTreePostTablesSpanningTreeInconsistenciesPortsMultipleNeighborsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpanningTree', 'postTablesSpanningTreeInconsistenciesPortsMultipleNeighbors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spanningTreePostTablesSpanningTreeInconsistenciesStpCdpPortsMismatchBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesSpanningTreeInconsistenciesStpCdpPortsMismatch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSpanningTreeInconsistenciesStpCdpPortsMismatch(spanningTreePostTablesSpanningTreeInconsistenciesStpCdpPortsMismatchBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpanningTree', 'postTablesSpanningTreeInconsistenciesStpCdpPortsMismatch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spanningTreePostTablesSpanningTreeInstancesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesSpanningTreeInstances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSpanningTreeInstances(spanningTreePostTablesSpanningTreeInstancesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpanningTree', 'postTablesSpanningTreeInstances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spanningTreePostTablesSpanningTreeNeighborsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesSpanningTreeNeighbors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSpanningTreeNeighbors(spanningTreePostTablesSpanningTreeNeighborsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpanningTree', 'postTablesSpanningTreeNeighbors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spanningTreePostTablesSpanningTreePortsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesSpanningTreePorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSpanningTreePorts(spanningTreePostTablesSpanningTreePortsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpanningTree', 'postTablesSpanningTreePorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spanningTreePostTablesSpanningTreeRadiusBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesSpanningTreeRadius - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSpanningTreeRadius(spanningTreePostTablesSpanningTreeRadiusBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpanningTree', 'postTablesSpanningTreeRadius', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spanningTreePostTablesSpanningTreeTopologyBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesSpanningTreeTopology - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSpanningTreeTopology(spanningTreePostTablesSpanningTreeTopologyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpanningTree', 'postTablesSpanningTreeTopology', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spanningTreePostTablesSpanningTreeVlansBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesSpanningTreeVlans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSpanningTreeVlans(spanningTreePostTablesSpanningTreeVlansBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpanningTree', 'postTablesSpanningTreeVlans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityAaaAccountingBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityAaaAccounting - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityAaaAccounting(securityPostTablesSecurityAaaAccountingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityAaaAccounting', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityAaaAuthenticationBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityAaaAuthentication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityAaaAuthentication(securityPostTablesSecurityAaaAuthenticationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityAaaAuthentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityAaaAuthorizationBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityAaaAuthorization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityAaaAuthorization(securityPostTablesSecurityAaaAuthorizationBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityAaaAuthorization', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityAaaLinesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityAaaLines - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityAaaLines(securityPostTablesSecurityAaaLinesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityAaaLines', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityAaaServersBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityAaaServers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityAaaServers(securityPostTablesSecurityAaaServersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityAaaServers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityAclInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityAclInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityAclInterfaces(securityPostTablesSecurityAclInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityAclInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityAclObjectGroupsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityAclObjectGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityAclObjectGroups(securityPostTablesSecurityAclObjectGroupsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityAclObjectGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityDhcpBindingsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityDhcpBindings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityDhcpBindings(securityPostTablesSecurityDhcpBindingsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityDhcpBindings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityDhcpSnoopingBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityDhcpSnooping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityDhcpSnooping(securityPostTablesSecurityDhcpSnoopingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityDhcpSnooping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityDmvpnBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityDmvpn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityDmvpn(securityPostTablesSecurityDmvpnBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityDmvpn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityEnabledTelnetBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityEnabledTelnet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityEnabledTelnet(securityPostTablesSecurityEnabledTelnetBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityEnabledTelnet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityIpsecGatewaysBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityIpsecGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityIpsecGateways(securityPostTablesSecurityIpsecGatewaysBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityIpsecGateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityIpsecTunnelsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityIpsecTunnels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityIpsecTunnels(securityPostTablesSecurityIpsecTunnelsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityIpsecTunnels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecuritySecurePortsDevicesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesSecuritySecurePortsDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecuritySecurePortsDevices(securityPostTablesSecuritySecurePortsDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecuritySecurePortsDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecuritySecurePortsInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesSecuritySecurePortsInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecuritySecurePortsInterfaces(securityPostTablesSecuritySecurePortsInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecuritySecurePortsInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecuritySecurePortsUsersBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesSecuritySecurePortsUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecuritySecurePortsUsers(securityPostTablesSecuritySecurePortsUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecuritySecurePortsUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityZoneFirewallInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityZoneFirewallInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityZoneFirewallInterfaces(securityPostTablesSecurityZoneFirewallInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityZoneFirewallInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityPostTablesSecurityZoneFirewallPoliciesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesSecurityZoneFirewallPolicies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesSecurityZoneFirewallPolicies(securityPostTablesSecurityZoneFirewallPoliciesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Security', 'postTablesSecurityZoneFirewallPolicies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksPostTablesNetworksDomainBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesNetworksDomain - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesNetworksDomain(networksPostTablesNetworksDomainBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'postTablesNetworksDomain', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksPostTablesNetworksPathLookupChecksBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesNetworksPathLookupChecks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesNetworksPathLookupChecks(networksPostTablesNetworksPathLookupChecksBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'postTablesNetworksPathLookupChecks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksPostTablesNetworksRouteStabilityBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesNetworksRouteStability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesNetworksRouteStability(networksPostTablesNetworksRouteStabilityBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'postTablesNetworksRouteStability', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksPostTablesNetworksRoutesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesNetworksRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesNetworksRoutes(networksPostTablesNetworksRoutesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'postTablesNetworksRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksPostTablesNetworksSummaryProtocolsBgpBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesNetworksSummaryProtocolsBgp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesNetworksSummaryProtocolsBgp(networksPostTablesNetworksSummaryProtocolsBgpBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'postTablesNetworksSummaryProtocolsBgp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksPostTablesNetworksSummaryProtocolsIsisBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesNetworksSummaryProtocolsIsis - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesNetworksSummaryProtocolsIsis(networksPostTablesNetworksSummaryProtocolsIsisBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'postTablesNetworksSummaryProtocolsIsis', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksPostTablesNetworksSummaryProtocolsOspfBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesNetworksSummaryProtocolsOspf - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesNetworksSummaryProtocolsOspf(networksPostTablesNetworksSummaryProtocolsOspfBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'postTablesNetworksSummaryProtocolsOspf', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networksPostTablesNetworksSummaryProtocolsOspfv3BodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesNetworksSummaryProtocolsOspfv3 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesNetworksSummaryProtocolsOspfv3(networksPostTablesNetworksSummaryProtocolsOspfv3BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Networks', 'postTablesNetworksSummaryProtocolsOspfv3', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aciPostTablesAciEndpointsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesAciEndpoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesAciEndpoints(aciPostTablesAciEndpointsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aci', 'postTablesAciEndpoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aciPostTablesAciVlanBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesAciVlan - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesAciVlan(aciPostTablesAciVlanBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aci', 'postTablesAciVlan', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const aciPostTablesAciVrfBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesAciVrf - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesAciVrf(aciPostTablesAciVrfBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Aci', 'postTablesAciVrf', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vrfPostTablesVrfDetailBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesVrfDetail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesVrfDetail(vrfPostTablesVrfDetailBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vrf', 'postTablesVrfDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vrfPostTablesVrfInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesVrfInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesVrfInterfaces(vrfPostTablesVrfInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vrf', 'postTablesVrfInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vrfPostTablesVrfSummaryBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesVrfSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesVrfSummary(vrfPostTablesVrfSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vrf', 'postTablesVrfSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wirelessPostTablesWirelessAccessPointsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesWirelessAccessPoints - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesWirelessAccessPoints(wirelessPostTablesWirelessAccessPointsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wireless', 'postTablesWirelessAccessPoints', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wirelessPostTablesWirelessClientsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesWirelessClients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesWirelessClients(wirelessPostTablesWirelessClientsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wireless', 'postTablesWirelessClients', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wirelessPostTablesWirelessControllersBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesWirelessControllers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesWirelessControllers(wirelessPostTablesWirelessControllersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wireless', 'postTablesWirelessControllers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wirelessPostTablesWirelessRadioBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesWirelessRadio - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesWirelessRadio(wirelessPostTablesWirelessRadioBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wireless', 'postTablesWirelessRadio', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wirelessPostTablesWirelessSsidSummaryBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesWirelessSsidSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesWirelessSsidSummary(wirelessPostTablesWirelessSsidSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wireless', 'postTablesWirelessSsidSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const neighborsPostTablesNeighborsAllBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesNeighborsAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesNeighborsAll(neighborsPostTablesNeighborsAllBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Neighbors', 'postTablesNeighborsAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const neighborsPostTablesNeighborsUnidirectionalBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesNeighborsUnidirectional - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesNeighborsUnidirectional(neighborsPostTablesNeighborsUnidirectionalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Neighbors', 'postTablesNeighborsUnidirectional', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const neighborsPostTablesNeighborsUnmanagedBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesNeighborsUnmanaged - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesNeighborsUnmanaged(neighborsPostTablesNeighborsUnmanagedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Neighbors', 'postTablesNeighborsUnmanaged', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mplsPostTablesMplsForwardingBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesMplsForwarding - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMplsForwarding(mplsPostTablesMplsForwardingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mpls', 'postTablesMplsForwarding', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mplsPostTablesMplsL2VpnCurcitCrossConnectBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesMplsL2VpnCurcitCrossConnect - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMplsL2VpnCurcitCrossConnect(mplsPostTablesMplsL2VpnCurcitCrossConnectBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mpls', 'postTablesMplsL2VpnCurcitCrossConnect', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mplsPostTablesMplsL2VpnPointToMultipointBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesMplsL2VpnPointToMultipoint - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMplsL2VpnPointToMultipoint(mplsPostTablesMplsL2VpnPointToMultipointBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mpls', 'postTablesMplsL2VpnPointToMultipoint', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mplsPostTablesMplsL2VpnPointToPointVpwsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesMplsL2VpnPointToPointVpws - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMplsL2VpnPointToPointVpws(mplsPostTablesMplsL2VpnPointToPointVpwsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mpls', 'postTablesMplsL2VpnPointToPointVpws', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mplsPostTablesMplsL2VpnPseudowiresBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesMplsL2VpnPseudowires - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMplsL2VpnPseudowires(mplsPostTablesMplsL2VpnPseudowiresBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mpls', 'postTablesMplsL2VpnPseudowires', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mplsPostTablesMplsL3VpnPeRoutersBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesMplsL3VpnPeRouters - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMplsL3VpnPeRouters(mplsPostTablesMplsL3VpnPeRoutersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mpls', 'postTablesMplsL3VpnPeRouters', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mplsPostTablesMplsL3VpnPeRoutesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesMplsL3VpnPeRoutes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMplsL3VpnPeRoutes(mplsPostTablesMplsL3VpnPeRoutesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mpls', 'postTablesMplsL3VpnPeRoutes', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mplsPostTablesMplsL3VpnPeVrfsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesMplsL3VpnPeVrfs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMplsL3VpnPeVrfs(mplsPostTablesMplsL3VpnPeVrfsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mpls', 'postTablesMplsL3VpnPeVrfs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mplsPostTablesMplsL3VpnVrfTargetsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesMplsL3VpnVrfTargets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMplsL3VpnVrfTargets(mplsPostTablesMplsL3VpnVrfTargetsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mpls', 'postTablesMplsL3VpnVrfTargets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mplsPostTablesMplsLdpInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesMplsLdpInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMplsLdpInterfaces(mplsPostTablesMplsLdpInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mpls', 'postTablesMplsLdpInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mplsPostTablesMplsLdpNeighborsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesMplsLdpNeighbors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesMplsLdpNeighbors(mplsPostTablesMplsLdpNeighborsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Mpls', 'postTablesMplsLdpNeighbors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsClusterSrxBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsClusterSrx - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsClusterSrx(platformsPostTablesPlatformsClusterSrxBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsClusterSrx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsFexInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsFexInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsFexInterfaces(platformsPostTablesPlatformsFexInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsFexInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsFexModulesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsFexModules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsFexModules(platformsPostTablesPlatformsFexModulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsFexModules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsPoeDevicesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsPoeDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsPoeDevices(platformsPostTablesPlatformsPoeDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsPoeDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsPoeInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsPoeInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsPoeInterfaces(platformsPostTablesPlatformsPoeInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsPoeInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsPoeModulesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsPoeModules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsPoeModules(platformsPostTablesPlatformsPoeModulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsPoeModules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsStackBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsStack(platformsPostTablesPlatformsStackBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsStack', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsStackConnectionsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsStackConnections - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsStackConnections(platformsPostTablesPlatformsStackConnectionsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsStackConnections', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsStackMembersBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsStackMembers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsStackMembers(platformsPostTablesPlatformsStackMembersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsStackMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsVdcDevicesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsVdcDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsVdcDevices(platformsPostTablesPlatformsVdcDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsVdcDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsVpcPairsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsVpcPairs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsVpcPairs(platformsPostTablesPlatformsVpcPairsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsVpcPairs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsVpcSwitchesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsVpcSwitches - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsVpcSwitches(platformsPostTablesPlatformsVpcSwitchesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsVpcSwitches', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsVssChassisBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsVssChassis - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsVssChassis(platformsPostTablesPlatformsVssChassisBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsVssChassis', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsVssOverviewBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsVssOverview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsVssOverview(platformsPostTablesPlatformsVssOverviewBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsVssOverview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const platformsPostTablesPlatformsVssVslBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesPlatformsVssVsl - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesPlatformsVssVsl(platformsPostTablesPlatformsVssVslBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Platforms', 'postTablesPlatformsVssVsl', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vlanPostTablesVlanDeviceBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesVlanDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesVlanDevice(vlanPostTablesVlanDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'postTablesVlanDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vlanPostTablesVlanDeviceSummaryBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesVlanDeviceSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesVlanDeviceSummary(vlanPostTablesVlanDeviceSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'postTablesVlanDeviceSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vlanPostTablesVlanL3GatewaysBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesVlanL3Gateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesVlanL3Gateways(vlanPostTablesVlanL3GatewaysBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'postTablesVlanL3Gateways', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vlanPostTablesVlanNetworkSummaryBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesVlanNetworkSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesVlanNetworkSummary(vlanPostTablesVlanNetworkSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'postTablesVlanNetworkSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vlanPostTablesVlanSiteSummaryBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesVlanSiteSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesVlanSiteSummary(vlanPostTablesVlanSiteSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vlan', 'postTablesVlanSiteSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fhrpPostTablesFhrpBalancingBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesFhrpBalancing - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesFhrpBalancing(fhrpPostTablesFhrpBalancingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Fhrp', 'postTablesFhrpBalancing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fhrpPostTablesFhrpGlbpForwardersBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesFhrpGlbpForwarders - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesFhrpGlbpForwarders(fhrpPostTablesFhrpGlbpForwardersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Fhrp', 'postTablesFhrpGlbpForwarders', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fhrpPostTablesFhrpGroupMembersBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesFhrpGroupMembers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesFhrpGroupMembers(fhrpPostTablesFhrpGroupMembersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Fhrp', 'postTablesFhrpGroupMembers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fhrpPostTablesFhrpGroupStateBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesFhrpGroupState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesFhrpGroupState(fhrpPostTablesFhrpGroupStateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Fhrp', 'postTablesFhrpGroupState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fhrpPostTablesFhrpStprootAlignmentBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesFhrpStprootAlignment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesFhrpStprootAlignment(fhrpPostTablesFhrpStprootAlignmentBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Fhrp', 'postTablesFhrpStprootAlignment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingPostTablesRoutingProtocolsBgpAddressFamiliesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesRoutingProtocolsBgpAddressFamilies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesRoutingProtocolsBgpAddressFamilies(routingPostTablesRoutingProtocolsBgpAddressFamiliesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'postTablesRoutingProtocolsBgpAddressFamilies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingPostTablesRoutingProtocolsBgpNeighborsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesRoutingProtocolsBgpNeighbors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesRoutingProtocolsBgpNeighbors(routingPostTablesRoutingProtocolsBgpNeighborsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'postTablesRoutingProtocolsBgpNeighbors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingPostTablesRoutingProtocolsEigrpInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesRoutingProtocolsEigrpInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesRoutingProtocolsEigrpInterfaces(routingPostTablesRoutingProtocolsEigrpInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'postTablesRoutingProtocolsEigrpInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingPostTablesRoutingProtocolsEigrpNeighborsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesRoutingProtocolsEigrpNeighbors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesRoutingProtocolsEigrpNeighbors(routingPostTablesRoutingProtocolsEigrpNeighborsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'postTablesRoutingProtocolsEigrpNeighbors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingPostTablesRoutingProtocolsIsIsInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesRoutingProtocolsIsIsInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesRoutingProtocolsIsIsInterfaces(routingPostTablesRoutingProtocolsIsIsInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'postTablesRoutingProtocolsIsIsInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingPostTablesRoutingProtocolsIsIsNeighborsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesRoutingProtocolsIsIsNeighbors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesRoutingProtocolsIsIsNeighbors(routingPostTablesRoutingProtocolsIsIsNeighborsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'postTablesRoutingProtocolsIsIsNeighbors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingPostTablesRoutingProtocolsOspfV3InterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesRoutingProtocolsOspfV3Interfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesRoutingProtocolsOspfV3Interfaces(routingPostTablesRoutingProtocolsOspfV3InterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'postTablesRoutingProtocolsOspfV3Interfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingPostTablesRoutingProtocolsOspfV3NeighborsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesRoutingProtocolsOspfV3Neighbors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesRoutingProtocolsOspfV3Neighbors(routingPostTablesRoutingProtocolsOspfV3NeighborsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'postTablesRoutingProtocolsOspfV3Neighbors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingPostTablesRoutingProtocolsOspfInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesRoutingProtocolsOspfInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesRoutingProtocolsOspfInterfaces(routingPostTablesRoutingProtocolsOspfInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'postTablesRoutingProtocolsOspfInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingPostTablesRoutingProtocolsOspfNeighborsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesRoutingProtocolsOspfNeighbors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesRoutingProtocolsOspfNeighbors(routingPostTablesRoutingProtocolsOspfNeighborsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'postTablesRoutingProtocolsOspfNeighbors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingPostTablesRoutingProtocolsRipInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesRoutingProtocolsRipInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesRoutingProtocolsRipInterfaces(routingPostTablesRoutingProtocolsRipInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'postTablesRoutingProtocolsRipInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const routingPostTablesRoutingProtocolsRipNeighborsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesRoutingProtocolsRipNeighbors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesRoutingProtocolsRipNeighbors(routingPostTablesRoutingProtocolsRipNeighborsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Routing', 'postTablesRoutingProtocolsRipNeighbors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementConfigurationSavedBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementConfigurationSaved - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementConfigurationSaved(managementPostTablesManagementConfigurationSavedBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementConfigurationSaved', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementDiscoveryRunsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementDiscoveryRuns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementDiscoveryRuns(managementPostTablesManagementDiscoveryRunsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementDiscoveryRuns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementFlowNetflowCollectorsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementFlowNetflowCollectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementFlowNetflowCollectors(managementPostTablesManagementFlowNetflowCollectorsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementFlowNetflowCollectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementFlowNetflowDevicesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementFlowNetflowDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementFlowNetflowDevices(managementPostTablesManagementFlowNetflowDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementFlowNetflowDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementFlowNetflowInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementFlowNetflowInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementFlowNetflowInterfaces(managementPostTablesManagementFlowNetflowInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementFlowNetflowInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementFlowOverviewBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementFlowOverview - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementFlowOverview(managementPostTablesManagementFlowOverviewBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementFlowOverview', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementFlowSflowCollectorsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementFlowSflowCollectors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementFlowSflowCollectors(managementPostTablesManagementFlowSflowCollectorsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementFlowSflowCollectors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementFlowSflowDevicesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementFlowSflowDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementFlowSflowDevices(managementPostTablesManagementFlowSflowDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementFlowSflowDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementFlowSflowSourcesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementFlowSflowSources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementFlowSflowSources(managementPostTablesManagementFlowSflowSourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementFlowSflowSources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementLoggingLocalBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementLoggingLocal - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementLoggingLocal(managementPostTablesManagementLoggingLocalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementLoggingLocal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementLoggingRemoteBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementLoggingRemote - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementLoggingRemote(managementPostTablesManagementLoggingRemoteBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementLoggingRemote', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementLoggingSummaryBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementLoggingSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementLoggingSummary(managementPostTablesManagementLoggingSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementLoggingSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementNtpSourcesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementNtpSources - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementNtpSources(managementPostTablesManagementNtpSourcesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementNtpSources', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementNtpSummaryBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementNtpSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementNtpSummary(managementPostTablesManagementNtpSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementNtpSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementOamUnidirectionalLinkDetectionInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementOamUnidirectionalLinkDetectionInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementOamUnidirectionalLinkDetectionInterfaces(managementPostTablesManagementOamUnidirectionalLinkDetectionInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementOamUnidirectionalLinkDetectionInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementOamUnidirectionalLinkDetectionNeighborsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementOamUnidirectionalLinkDetectionNeighbors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementOamUnidirectionalLinkDetectionNeighbors(managementPostTablesManagementOamUnidirectionalLinkDetectionNeighborsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementOamUnidirectionalLinkDetectionNeighbors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementOsverConsistencyBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementOsverConsistency - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementOsverConsistency(managementPostTablesManagementOsverConsistencyBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementOsverConsistency', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementPortMirroringBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementPortMirroring - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementPortMirroring(managementPostTablesManagementPortMirroringBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementPortMirroring', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementSnmpCommunitiesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementSnmpCommunities - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementSnmpCommunities(managementPostTablesManagementSnmpCommunitiesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementSnmpCommunities', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementSnmpSummaryBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementSnmpSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementSnmpSummary(managementPostTablesManagementSnmpSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementSnmpSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementSnmpTrapHostsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementSnmpTrapHosts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementSnmpTrapHosts(managementPostTablesManagementSnmpTrapHostsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementSnmpTrapHosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const managementPostTablesManagementSnmpUsersBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesManagementSnmpUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesManagementSnmpUsers(managementPostTablesManagementSnmpUsersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'postTablesManagementSnmpUsers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesConnectivityMatrixUnmanagedNeighborsDetailBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesConnectivityMatrixUnmanagedNeighborsDetail - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesConnectivityMatrixUnmanagedNeighborsDetail(interfacesPostTablesInterfacesConnectivityMatrixUnmanagedNeighborsDetailBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesConnectivityMatrixUnmanagedNeighborsDetail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesConnectivityMatrixUnmanagedNeighborsSummaryBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesConnectivityMatrixUnmanagedNeighborsSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesConnectivityMatrixUnmanagedNeighborsSummary(interfacesPostTablesInterfacesConnectivityMatrixUnmanagedNeighborsSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesConnectivityMatrixUnmanagedNeighborsSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesDropsBidirectionalBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesDropsBidirectional - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesDropsBidirectional(interfacesPostTablesInterfacesDropsBidirectionalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesDropsBidirectional', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesDropsBidirectionalDeviceBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesDropsBidirectionalDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesDropsBidirectionalDevice(interfacesPostTablesInterfacesDropsBidirectionalDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesDropsBidirectionalDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesDropsInboundBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesDropsInbound - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesDropsInbound(interfacesPostTablesInterfacesDropsInboundBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesDropsInbound', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesDropsInboundDeviceBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesDropsInboundDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesDropsInboundDevice(interfacesPostTablesInterfacesDropsInboundDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesDropsInboundDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesDropsOutboundBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesDropsOutbound - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesDropsOutbound(interfacesPostTablesInterfacesDropsOutboundBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesDropsOutbound', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesDropsOutboundDeviceBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesDropsOutboundDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesDropsOutboundDevice(interfacesPostTablesInterfacesDropsOutboundDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesDropsOutboundDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesDuplexBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesDuplex - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesDuplex(interfacesPostTablesInterfacesDuplexBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesDuplex', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesErrorsBidirectionalBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesErrorsBidirectional - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesErrorsBidirectional(interfacesPostTablesInterfacesErrorsBidirectionalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesErrorsBidirectional', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesErrorsBidirectionalDeviceBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesErrorsBidirectionalDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesErrorsBidirectionalDevice(interfacesPostTablesInterfacesErrorsBidirectionalDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesErrorsBidirectionalDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesErrorsDisabledBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesErrorsDisabled - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesErrorsDisabled(interfacesPostTablesInterfacesErrorsDisabledBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesErrorsDisabled', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesErrorsInboundBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesErrorsInbound - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesErrorsInbound(interfacesPostTablesInterfacesErrorsInboundBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesErrorsInbound', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesErrorsInboundDeviceBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesErrorsInboundDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesErrorsInboundDevice(interfacesPostTablesInterfacesErrorsInboundDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesErrorsInboundDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesErrorsOutboundBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesErrorsOutbound - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesErrorsOutbound(interfacesPostTablesInterfacesErrorsOutboundBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesErrorsOutbound', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesErrorsOutboundDeviceBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesErrorsOutboundDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesErrorsOutboundDevice(interfacesPostTablesInterfacesErrorsOutboundDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesErrorsOutboundDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesInconsistenciesDetailsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesInconsistenciesDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesInconsistenciesDetails(interfacesPostTablesInterfacesInconsistenciesDetailsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesInconsistenciesDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesInconsistenciesSummaryBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesInconsistenciesSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesInconsistenciesSummary(interfacesPostTablesInterfacesInconsistenciesSummaryBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesInconsistenciesSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesLoadBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesLoad - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesLoad(interfacesPostTablesInterfacesLoadBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesLoad', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesMtuBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesMtu - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesMtu(interfacesPostTablesInterfacesMtuBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesMtu', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesPortChannelBalanceInboundBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesPortChannelBalanceInbound - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesPortChannelBalanceInbound(interfacesPostTablesInterfacesPortChannelBalanceInboundBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesPortChannelBalanceInbound', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesPortChannelBalanceOutboundBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesPortChannelBalanceOutbound - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesPortChannelBalanceOutbound(interfacesPostTablesInterfacesPortChannelBalanceOutboundBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesPortChannelBalanceOutbound', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesPortChannelMemberStatusBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesPortChannelMemberStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesPortChannelMemberStatus(interfacesPostTablesInterfacesPortChannelMemberStatusBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesPortChannelMemberStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesStormControlBroadcastBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesStormControlBroadcast - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesStormControlBroadcast(interfacesPostTablesInterfacesStormControlBroadcastBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesStormControlBroadcast', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesStormControlMulticastBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesStormControlMulticast - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesStormControlMulticast(interfacesPostTablesInterfacesStormControlMulticastBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesStormControlMulticast', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesStormControlUnicastBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesStormControlUnicast - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesStormControlUnicast(interfacesPostTablesInterfacesStormControlUnicastBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesStormControlUnicast', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesSwitchportsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesSwitchports - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesSwitchports(interfacesPostTablesInterfacesSwitchportsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesSwitchports', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesTransferRatesBidirectionalBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesTransferRatesBidirectional - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesTransferRatesBidirectional(interfacesPostTablesInterfacesTransferRatesBidirectionalBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesTransferRatesBidirectional', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesTransferRatesBidirectionalDeviceBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesTransferRatesBidirectionalDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesTransferRatesBidirectionalDevice(interfacesPostTablesInterfacesTransferRatesBidirectionalDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesTransferRatesBidirectionalDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesTransferRatesInboundBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesTransferRatesInbound - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesTransferRatesInbound(interfacesPostTablesInterfacesTransferRatesInboundBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesTransferRatesInbound', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesTransferRatesInboundDeviceBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesTransferRatesInboundDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesTransferRatesInboundDevice(interfacesPostTablesInterfacesTransferRatesInboundDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesTransferRatesInboundDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesTransferRatesOutboundBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesTransferRatesOutbound - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesTransferRatesOutbound(interfacesPostTablesInterfacesTransferRatesOutboundBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesTransferRatesOutbound', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfacesPostTablesInterfacesTransferRatesOutboundDeviceBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesInterfacesTransferRatesOutboundDevice - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInterfacesTransferRatesOutboundDevice(interfacesPostTablesInterfacesTransferRatesOutboundDeviceBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Interfaces', 'postTablesInterfacesTransferRatesOutboundDevice', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressingPostTablesAddressingArpBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesAddressingArp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesAddressingArp(addressingPostTablesAddressingArpBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addressing', 'postTablesAddressingArp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressingPostTablesAddressingDuplicateIpBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesAddressingDuplicateIp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesAddressingDuplicateIp(addressingPostTablesAddressingDuplicateIpBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addressing', 'postTablesAddressingDuplicateIp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressingPostTablesAddressingHostsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesAddressingHosts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesAddressingHosts(addressingPostTablesAddressingHostsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addressing', 'postTablesAddressingHosts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressingPostTablesAddressingMacBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesAddressingMac - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesAddressingMac(addressingPostTablesAddressingMacBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addressing', 'postTablesAddressingMac', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressingPostTablesAddressingManagedDevsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesAddressingManagedDevs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesAddressingManagedDevs(addressingPostTablesAddressingManagedDevsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addressing', 'postTablesAddressingManagedDevs', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressingPostTablesAddressingNatPoolsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesAddressingNatPools - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesAddressingNatPools(addressingPostTablesAddressingNatPoolsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addressing', 'postTablesAddressingNatPools', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const addressingPostTablesAddressingNatRulesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesAddressingNatRules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesAddressingNatRules(addressingPostTablesAddressingNatRulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Addressing', 'postTablesAddressingNatRules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vxlanPostTablesVxlanInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesVxlanInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesVxlanInterfaces(vxlanPostTablesVxlanInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vxlan', 'postTablesVxlanInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vxlanPostTablesVxlanPeersBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesVxlanPeers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesVxlanPeers(vxlanPostTablesVxlanPeersBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vxlan', 'postTablesVxlanPeers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vxlanPostTablesVxlanVniBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesVxlanVni - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesVxlanVni(vxlanPostTablesVxlanVniBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vxlan', 'postTablesVxlanVni', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vxlanPostTablesVxlanVtepBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesVxlanVtep - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesVxlanVtep(vxlanPostTablesVxlanVtepBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vxlan', 'postTablesVxlanVtep', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const qosPostTablesQosMarkingBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesQosMarking - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesQosMarking(qosPostTablesQosMarkingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Qos', 'postTablesQosMarking', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const qosPostTablesQosPolicingBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesQosPolicing - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesQosPolicing(qosPostTablesQosPolicingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Qos', 'postTablesQosPolicing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const qosPostTablesQosPolicyMapsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesQosPolicyMaps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesQosPolicyMaps(qosPostTablesQosPolicyMapsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Qos', 'postTablesQosPolicyMaps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const qosPostTablesQosPriorityQueuingBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 5
      },
      snapshot: '$last'
    };
    describe('#postTablesQosPriorityQueuing - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesQosPriorityQueuing(qosPostTablesQosPriorityQueuingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Qos', 'postTablesQosPriorityQueuing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const qosPostTablesQosQueuingBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesQosQueuing - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesQosQueuing(qosPostTablesQosQueuingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Qos', 'postTablesQosQueuing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const qosPostTablesQosShapingBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesQosShaping - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesQosShaping(qosPostTablesQosShapingBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Qos', 'postTablesQosShaping', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventoryDevicesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesInventoryDevices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventoryDevices(inventoryPostTablesInventoryDevicesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventoryDevices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventoryFansBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 9
      },
      snapshot: '$last'
    };
    describe('#postTablesInventoryFans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventoryFans(inventoryPostTablesInventoryFansBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventoryFans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventoryInterfacesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesInventoryInterfaces - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventoryInterfaces(inventoryPostTablesInventoryInterfacesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventoryInterfaces', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventoryModulesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 7
      },
      snapshot: '$last'
    };
    describe('#postTablesInventoryModules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventoryModules(inventoryPostTablesInventoryModulesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventoryModules', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventoryPhonesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 6
      },
      snapshot: '$last'
    };
    describe('#postTablesInventoryPhones - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventoryPhones(inventoryPostTablesInventoryPhonesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventoryPhones', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventoryPnBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesInventoryPn - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventoryPn(inventoryPostTablesInventoryPnBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventoryPn', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventoryPowerSuppliesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      snapshot: '$last'
    };
    describe('#postTablesInventoryPowerSupplies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventoryPowerSupplies(inventoryPostTablesInventoryPowerSuppliesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventoryPowerSupplies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventoryPowerSuppliesFansBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 2
      },
      snapshot: '$last'
    };
    describe('#postTablesInventoryPowerSuppliesFans - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventoryPowerSuppliesFans(inventoryPostTablesInventoryPowerSuppliesFansBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventoryPowerSuppliesFans', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventorySitesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 1
      },
      snapshot: '$last'
    };
    describe('#postTablesInventorySites - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventorySites(inventoryPostTablesInventorySitesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventorySites', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventorySummaryFamiliesBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 3
      },
      snapshot: '$last'
    };
    describe('#postTablesInventorySummaryFamilies - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventorySummaryFamilies(inventoryPostTablesInventorySummaryFamiliesBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventorySummaryFamilies', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventorySummaryModelsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesInventorySummaryModels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventorySummaryModels(inventoryPostTablesInventorySummaryModelsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventorySummaryModels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventorySummaryPlatformsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 10
      },
      snapshot: '$last'
    };
    describe('#postTablesInventorySummaryPlatforms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventorySummaryPlatforms(inventoryPostTablesInventorySummaryPlatformsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventorySummaryPlatforms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryPostTablesInventorySummaryVendorsBodyParam = {
      filters: {},
      pagination: {
        limit: 10,
        start: 8
      },
      snapshot: '$last'
    };
    describe('#postTablesInventorySummaryVendors - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postTablesInventorySummaryVendors(inventoryPostTablesInventorySummaryVendorsBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response._meta);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventory', 'postTablesInventorySummaryVendors', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryDiffBodyParam = {
      fake: 'data'
    };
    describe('#inventoryDiff - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoryDiff(inventoryDiffBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'inventoryDiff', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deviceDiffBodyParam = {
      bindVariables: {
        after: 'b186fcb0',
        before: '22fc2140'
      },
      filters: {},
      pagination: {
        limit: 10,
        start: 4
      },
      reports: 'fakeData'
    };
    describe('#deviceDiff - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deviceDiff(deviceDiffBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Management', 'deviceDiff', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersUserId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUsersUserId(usersUserId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUsersUserId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthToken - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteAuthToken((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'deleteAuthToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSnapshot - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSnapshot(snapshotsSnapshotId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snapshots', 'deleteSnapshot', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSettingsCredentialsCredentialsId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSettingsCredentialsCredentialsId(settingsCredentialsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'deleteSettingsCredentialsCredentialsId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteEnablePassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deleteEnablePassword(settingsPrivilegesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'deleteEnablePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteSiteSeparationSettings - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteSiteSeparationSettings(settingsRuleId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ipfabric-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'deleteSiteSeparationSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
