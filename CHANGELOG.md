
## 1.2.5 [10-15-2024]

* Changes made at 2024.10.14_20:30PM

See merge request itentialopensource/adapters/adapter-ipfabric!21

---

## 1.2.4 [08-30-2024]

* fix vulnerability

See merge request itentialopensource/adapters/adapter-ipfabric!19

---

## 1.2.3 [08-28-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-ipfabric!18

---

## 1.2.2 [08-14-2024]

* Changes made at 2024.08.14_18:43PM

See merge request itentialopensource/adapters/adapter-ipfabric!17

---

## 1.2.1 [08-07-2024]

* Changes made at 2024.08.06_19:57PM

See merge request itentialopensource/adapters/adapter-ipfabric!16

---

## 1.2.0 [08-05-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-ipfabric!15

---

## 1.1.5 [03-27-2024]

* Changes made at 2024.03.27_13:16PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-ipfabric!14

---

## 1.1.4 [03-14-2024]

* Update metadata.json

See merge request itentialopensource/adapters/telemetry-analytics/adapter-ipfabric!13

---

## 1.1.3 [03-12-2024]

* Changes made at 2024.03.12_11:00AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-ipfabric!12

---

## 1.1.2 [02-27-2024]

* Changes made at 2024.02.27_11:35AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-ipfabric!11

---

## 1.1.1 [01-05-2024]

* more migration changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-ipfabric!10

---

## 1.1.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-ipfabric!8

---

## 1.0.2 [04-28-2023]

* Bug fixes and performance improvements

See commit 032bb7c

---

## 1.0.1 [04-28-2023]

* Patch/adapt 2503

See merge request itentialopensource/adapters/telemetry-analytics/adapter-ipfabric!7

---

## 1.0.0 [12-05-2022]

* major/ADAPT-2434

See merge request itentialopensource/adapters/telemetry-analytics/adapter-ipfabric!5

---

## 0.3.1 [05-18-2022]

* Fix systemName

See merge request itentialopensource/adapters/telemetry-analytics/adapter-ipfabric!3

---

## 0.3.0 [05-18-2022] & 0.2.0 [01-19-2022]

- changes based in information from ipfabric and also migration of the adapter.
  - Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
  - Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
  - Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
  - Add scripts for easier authentication, removing hooks, etc
  - Script changes (install script as well as database changes in other scripts)
  - Double # of path vars on generic call
  - Update versions of foundation (e.g. adapter-utils)
  - Update npm publish so it supports https
  - Update dependencies
  - Add preinstall for minimist
  - Fix new lint issues that came from eslint dependency change
  - Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
  - Add the adapter type in the package.json
  - Add AdapterInfo.js script
  - Add json-query dependency
  - Add the propertiesDecorators.json for product encryption
  - Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
  - Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
  - Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
  - Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
  - Add AdapterInfo.json
  - Add systemName for documentation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-ipfabric!1

---

## 0.1.1 [06-14-2021]

- Initial Commit

See commit 8305aed

---
