## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.


### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for IP Fabric. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for IP Fabric.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>


### Specific Adapter Calls

Specific adapter calls are built based on the API of the IpFabric. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getOsVersion(callback)</td>
    <td style="padding:15px">Provides basic information about the IP Fabric system version</td>
    <td style="padding:15px">{base_path}/{version}/os/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOsHostname(callback)</td>
    <td style="padding:15px">Provides hostname information</td>
    <td style="padding:15px">{base_path}/{version}/os/hostname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSupportStatus(callback)</td>
    <td style="padding:15px">Provides basic information about the IP Fabric system</td>
    <td style="padding:15px">{base_path}/{version}/support/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOsServiceLogServiceIdSnapshotId(serviceId = 'worker', snapshotId, callback)</td>
    <td style="padding:15px">Provides discovery logs for modules</td>
    <td style="padding:15px">{base_path}/{version}/os/service-log/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOsCleanDb(body, callback)</td>
    <td style="padding:15px">Cleans system database and/or restores to default</td>
    <td style="padding:15px">{base_path}/{version}/os/clean-db?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOsMaintenance(callback)</td>
    <td style="padding:15px">Initiate system Maintenance</td>
    <td style="padding:15px">{base_path}/{version}/os/maintenance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOsTechsupport(body, callback)</td>
    <td style="padding:15px">Generates the techsupport</td>
    <td style="padding:15px">{base_path}/{version}/os/techsupport?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOsGenerateNimpeeCert(body, callback)</td>
    <td style="padding:15px">Generates IP Fabric certificate</td>
    <td style="padding:15px">{base_path}/{version}/os/generate-nimpee-cert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOsUploadNimpeeCert(certificate, callback)</td>
    <td style="padding:15px">Uploads certificate to IP Fabric</td>
    <td style="padding:15px">{base_path}/{version}/os/upload-nimpee-cert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesJobs(body, callback)</td>
    <td style="padding:15px">Provides the list of available system jobs</td>
    <td style="padding:15px">{base_path}/{version}/tables/jobs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postJobsJobIdCancel(jobId, callback)</td>
    <td style="padding:15px">To CANCEL scheduled system jobs</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postJobsJobIdStop(jobId, callback)</td>
    <td style="padding:15px">To STOP currently running system jobs</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getJobsJobIdDownload(jobId, callback)</td>
    <td style="padding:15px">To download result of a system job [usually a Techsupport or Snapshot file]</td>
    <td style="padding:15px">{base_path}/{version}/jobs/{pathv1}/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesUsers(body, callback)</td>
    <td style="padding:15px">Provides the list of available users</td>
    <td style="padding:15px">{base_path}/{version}/tables/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsers(callback)</td>
    <td style="padding:15px">Provides basic information about users</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsers(body, callback)</td>
    <td style="padding:15px">Creates new user</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUserId(userId, callback)</td>
    <td style="padding:15px">Provides basic information about the user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUsersUserId(userId, callback)</td>
    <td style="padding:15px">TO BE COMPLETED</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersUserId(userId, callback)</td>
    <td style="padding:15px">Deletes user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersUserIdPassword(userId, body, callback)</td>
    <td style="padding:15px">TO BE COMPLETED</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersMe(callback)</td>
    <td style="padding:15px">Provides basic information about the authenticated user</td>
    <td style="padding:15px">{base_path}/{version}/users/me?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">patchUsersMe(callback)</td>
    <td style="padding:15px">TO BE COMPLETED</td>
    <td style="padding:15px">{base_path}/{version}/users/me?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersMePassword(callback)</td>
    <td style="padding:15px">Updates the password of authenticated user</td>
    <td style="padding:15px">{base_path}/{version}/users/me/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersMeSettings(callback)</td>
    <td style="padding:15px">Provides full settings/modifications for authenticated user</td>
    <td style="padding:15px">{base_path}/{version}/users/me/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthLogin(body, callback)</td>
    <td style="padding:15px">Authenticate the system to get the access and refresh tokens</td>
    <td style="padding:15px">{base_path}/{version}/auth/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthLogout(callback)</td>
    <td style="padding:15px">Logs user out of the system</td>
    <td style="padding:15px">{base_path}/{version}/auth/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthToken(body, callback)</td>
    <td style="padding:15px">Generate new access token by using a refresh token.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAuthToken(callback)</td>
    <td style="padding:15px">To revoke a refresh token.</td>
    <td style="padding:15px">{base_path}/{version}/auth/token?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSnapshots(callback)</td>
    <td style="padding:15px">Getting information about available snapshots</td>
    <td style="padding:15px">{base_path}/{version}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createSnapshot(body, callback)</td>
    <td style="padding:15px">Create new snapshot with global or new settings</td>
    <td style="padding:15px">{base_path}/{version}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">renameSnapshot(snapshotId, body, callback)</td>
    <td style="padding:15px">Update snapshot name or note</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSnapshot(snapshotId, callback)</td>
    <td style="padding:15px">Delete snapshot</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readSnapshotSettings(snapshotId, callback)</td>
    <td style="padding:15px">Read snapshot settings</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSnapshotSettings(snapshotId, body, callback)</td>
    <td style="padding:15px">Modify subset of snapshot settings</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lockSnapshot(snapshotId, callback)</td>
    <td style="padding:15px">Lock the snapshot in loaded area</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}/lock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlockSnapshots(snapshotId, callback)</td>
    <td style="padding:15px">Unlock the snapshot in loaded area</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}/unlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loadSnapshot(snapshotId, callback)</td>
    <td style="padding:15px">Load snapshot to Random Access Memory (RAM)</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}/load?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unloadSnapshot(snapshotId, callback)</td>
    <td style="padding:15px">Unload snapshot from Random Access Memory (RAM)</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}/unload?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadSnapshot(snapshotId, callback)</td>
    <td style="padding:15px">Initiate the snapshot download job</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}/download?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloneSnapshot(snapshotId, callback)</td>
    <td style="padding:15px">Clone snapshot and save as unloaded</td>
    <td style="padding:15px">{base_path}/{version}/snapshots/{pathv1}/clone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readGlobalSettings(callback)</td>
    <td style="padding:15px">Read global settings</td>
    <td style="padding:15px">{base_path}/{version}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateGlobalSettings(callback)</td>
    <td style="padding:15px">Modify global settings</td>
    <td style="padding:15px">{base_path}/{version}/settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readEnablePasswords(callback)</td>
    <td style="padding:15px">Read global enable passwords</td>
    <td style="padding:15px">{base_path}/{version}/settings/privileges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addEnablePassword(body, callback)</td>
    <td style="padding:15px">Add global enable password</td>
    <td style="padding:15px">{base_path}/{version}/settings/privileges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateEnablePassword(privilegesId, callback)</td>
    <td style="padding:15px">Update global enable password settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/privileges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEnablePassword(privilegesId, callback)</td>
    <td style="padding:15px">NOT OPERATIONAL - Delete global enable password</td>
    <td style="padding:15px">{base_path}/{version}/settings/privileges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readCredentials(callback)</td>
    <td style="padding:15px">Read global credentials settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addCredentials(body, callback)</td>
    <td style="padding:15px">Read global credentials settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateCredentialSettings(credentialsId, callback)</td>
    <td style="padding:15px">Update user in global authentication settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSettingsCredentialsCredentialsId(credentialsId, callback)</td>
    <td style="padding:15px">Delete user from global authentication settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/credentials/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSeedSettings(callback)</td>
    <td style="padding:15px">Read global seed IP settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/seed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSeedSettings(body, callback)</td>
    <td style="padding:15px">Update global seed IP settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/seed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rewriteSeedSettings(callback)</td>
    <td style="padding:15px">Rewrite global seed IP settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/seed?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readSiteSeparationSettings(callback)</td>
    <td style="padding:15px">Read global site-separation settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/site-separation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteSeparationSettings(body, callback)</td>
    <td style="padding:15px">Modify global site-separation settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/site-separation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateSiteSeparationRule(ruleId, callback)</td>
    <td style="padding:15px">Update site separation single rule settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/site-separation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteSiteSeparationSettings(ruleId, callback)</td>
    <td style="padding:15px">Delete site separation settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/site-separation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">readVendorApiSettings(callback)</td>
    <td style="padding:15px">Read global vendor-api settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/vendor-api?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateJumphostSettings(callback)</td>
    <td style="padding:15px">Update jumphost global settings</td>
    <td style="padding:15px">{base_path}/{version}/settings/jumphosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">simulatePath(source, destination, sourcePort, destinationPort, asymmetric, rpf, protocol = 'udp', snapshot, callback)</td>
    <td style="padding:15px">End to End application path simulation</td>
    <td style="padding:15px">{base_path}/{version}/graph/end-to-end-path?{query}</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">pathLookup(body, callback)</td>
    <td style="padding:15px">End to End application path lookup</td>
    <td style="padding:15px">{base_path}/{version}/graphs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pathLookupSvg(body, callback)</td>
    <td style="padding:15px">End to End application path lookup</td>
    <td style="padding:15px">{base_path}/{version}/graphs/svg?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pathLookupPng(body, callback)</td>
    <td style="padding:15px">End to End application path lookup</td>
    <td style="padding:15px">{base_path}/{version}/graphs/png?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastIgmpInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/igmp/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastIgmpSnoopingVlans(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/igmp/snooping/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastIgmpSnoopingGlobal(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/igmp/snooping/global?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastIgmpSnoopingGroups(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/igmp/snooping/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastIgmpGroups(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/igmp/groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastPimInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/pim/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastPimRpMappings(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/pim/rp/mappings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastPimRpOverview(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/pim/rp/overview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastPimRpMappingsGroups(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/pim/rp/mappings-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastPimRpBsr(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/pim/rp/bsr?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastPimNeighbors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/pim/neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastMac(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/mac?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastRoutesTable(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/routes/table?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastRoutesSources(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/routes/sources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastRoutesCounters(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/routes/counters?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastRoutesOutgoingInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/routes/outgoing-interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastRoutesFirstHopRouter(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/routes/first-hop-router?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMulticastRoutesOverview(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/multicast/routes/overview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSpanningTreeBridges(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/spanning-tree/bridges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSpanningTreeTopology(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/spanning-tree/topology?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSpanningTreePorts(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/spanning-tree/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSpanningTreeInstances(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/spanning-tree/instances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSpanningTreeGuards(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/spanning-tree/guards?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSpanningTreeNeighbors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/spanning-tree/neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSpanningTreeVlans(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/spanning-tree/vlans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSpanningTreeRadius(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/spanning-tree/radius?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSpanningTreeInconsistenciesNeighborPortsVlanMismatch(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/spanning-tree/inconsistencies/neighbor-ports-vlan-mismatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSpanningTreeInconsistenciesMultipleStp(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/spanning-tree/inconsistencies/multiple-stp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSpanningTreeInconsistenciesPortsMultipleNeighbors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/spanning-tree/inconsistencies/ports-multiple-neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSpanningTreeInconsistenciesStpCdpPortsMismatch(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/spanning-tree/inconsistencies/stp-cdp-ports-mismatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecuritySecurePortsInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/secure-ports/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecuritySecurePortsUsers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/secure-ports/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecuritySecurePortsDevices(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/secure-ports/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityZoneFirewallInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/zone-firewall/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityZoneFirewallPolicies(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/zone-firewall/policies?{query}</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityAclObjectGroups(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/acl/object-groups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityAclInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/acl/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityAaaLines(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/aaa/lines?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityAaaAccounting(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/aaa/accounting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityAaaAuthentication(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/aaa/authentication?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityAaaAuthorization(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/aaa/authorization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityAaaServers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/aaa/servers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityDhcpSnooping(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/dhcp/snooping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityDhcpBindings(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/dhcp/bindings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityEnabledTelnet(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/enabled-telnet?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityIpsecGateways(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/ipsec/gateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityIpsecTunnels(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/ipsec/tunnels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesSecurityDmvpn(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/security/dmvpn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesNetworksRoutes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/networks/routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesNetworksRouteStability(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/networks/route-stability?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesNetworksSummaryProtocolsBgp(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/networks/summary/protocols/bgp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesNetworksSummaryProtocolsOspf(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/networks/summary/protocols/ospf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesNetworksSummaryProtocolsIsis(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/networks/summary/protocols/isis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesNetworksSummaryProtocolsOspfv3(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/networks/summary/protocols/ospfv3?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesNetworksDomain(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/networks/domain?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesNetworksPathLookupChecks(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/networks/path-lookup-checks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesAciEndpoints(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/aci/endpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesAciVrf(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/aci/vrf?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesAciVlan(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/aci/vlan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesVrfInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/vrf/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesVrfDetail(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/vrf/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesVrfSummary(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/vrf/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesWirelessRadio(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/wireless/radio?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesWirelessSsidSummary(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/wireless/ssid-summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesWirelessClients(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/wireless/clients?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesWirelessAccessPoints(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/wireless/access-points?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesWirelessControllers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/wireless/controllers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesNeighborsAll(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/neighbors/all?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesNeighborsUnidirectional(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/neighbors/unidirectional?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesNeighborsUnmanaged(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/neighbors/unmanaged?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMplsForwarding(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/mpls/forwarding?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMplsL2VpnPseudowires(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/mpls/l2-vpn/pseudowires?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMplsL2VpnPointToPointVpws(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/mpls/l2-vpn/point-to-point-vpws?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMplsL2VpnCurcitCrossConnect(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/mpls/l2-vpn/curcit-cross-connect?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMplsL2VpnPointToMultipoint(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/mpls/l2-vpn/point-to-multipoint?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMplsLdpInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/mpls/ldp/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMplsLdpNeighbors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/mpls/ldp/neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMplsL3VpnPeRoutes(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/mpls/l3-vpn/pe-routes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMplsL3VpnPeRouters(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/mpls/l3-vpn/pe-routers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMplsL3VpnPeVrfs(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/mpls/l3-vpn/pe-vrfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesMplsL3VpnVrfTargets(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/mpls/l3-vpn/vrf-targets?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsVpcPairs(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/vpc/pairs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsVpcSwitches(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/vpc/switches?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsStackConnections(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/stack/connections?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsStack(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/stack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsStackMembers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/stack/members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsFexModules(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/fex/modules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsFexInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/fex/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsClusterSrx(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/cluster/srx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsPoeModules(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/poe/modules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsPoeInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/poe/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsPoeDevices(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/poe/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsVssVsl(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/vss/vsl?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsVssChassis(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/vss/chassis?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsVssOverview(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/vss/overview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesPlatformsVdcDevices(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/platforms/vdc/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesVlanDevice(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/vlan/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesVlanL3Gateways(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/vlan/l3-gateways?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesVlanSiteSummary(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/vlan/site-summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesVlanNetworkSummary(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/vlan/network-summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesVlanDeviceSummary(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/vlan/device-summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesFhrpBalancing(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/fhrp/balancing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesFhrpGlbpForwarders(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/fhrp/glbp-forwarders?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesFhrpGroupMembers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/fhrp/group-members?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesFhrpStprootAlignment(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/fhrp/stproot-alignment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesFhrpGroupState(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/fhrp/group-state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesRoutingProtocolsBgpNeighbors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/routing/protocols/bgp/neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesRoutingProtocolsBgpAddressFamilies(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/routing/protocols/bgp/address-families?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesRoutingProtocolsOspfV3Interfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/routing/protocols/ospf-v3/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesRoutingProtocolsOspfV3Neighbors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/routing/protocols/ospf-v3/neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesRoutingProtocolsOspfInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/routing/protocols/ospf/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesRoutingProtocolsOspfNeighbors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/routing/protocols/ospf/neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesRoutingProtocolsRipInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/routing/protocols/rip/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesRoutingProtocolsRipNeighbors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/routing/protocols/rip/neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesRoutingProtocolsEigrpInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/routing/protocols/eigrp/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesRoutingProtocolsEigrpNeighbors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/routing/protocols/eigrp/neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesRoutingProtocolsIsIsInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/routing/protocols/is-is/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesRoutingProtocolsIsIsNeighbors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/routing/protocols/is-is/neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementLoggingRemote(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/logging/remote?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementLoggingLocal(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/logging/local?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementLoggingSummary(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/logging/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementOamUnidirectionalLinkDetectionInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/oam/unidirectional-link-detection/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementOamUnidirectionalLinkDetectionNeighbors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/oam/unidirectional-link-detection/neighbors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementFlowNetflowInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/flow/netflow/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementFlowNetflowCollectors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/flow/netflow/collectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementFlowNetflowDevices(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/flow/netflow/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementFlowSflowCollectors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/flow/sflow/collectors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementFlowSflowSources(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/flow/sflow/sources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementFlowSflowDevices(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/flow/sflow/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementFlowOverview(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/flow/overview?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementSnmpUsers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/snmp/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementSnmpCommunities(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/snmp/communities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementSnmpSummary(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/snmp/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementSnmpTrapHosts(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/snmp/trap-hosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementNtpSources(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/ntp/sources?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementNtpSummary(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/ntp/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementOsverConsistency(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/osver-consistency?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementDiscoveryRuns(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/discovery-runs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementPortMirroring(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/port-mirroring?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesManagementConfigurationSaved(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/management/configuration/saved?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesDropsInbound(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/drops/inbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesDropsBidirectionalDevice(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/drops/bidirectional-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesDropsInboundDevice(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/drops/inbound-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesDropsOutboundDevice(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/drops/outbound-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesDropsBidirectional(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/drops/bidirectional?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesDropsOutbound(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/drops/outbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesLoad(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/load?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesErrorsDisabled(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/errors/disabled?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesErrorsInbound(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/errors/inbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesErrorsBidirectionalDevice(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/errors/bidirectional-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesErrorsInboundDevice(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/errors/inbound-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesErrorsOutboundDevice(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/errors/outbound-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesErrorsBidirectional(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/errors/bidirectional?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesErrorsOutbound(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/errors/outbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesSwitchports(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/switchports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesTransferRatesInbound(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/transfer-rates/inbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesTransferRatesBidirectionalDevice(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/transfer-rates/bidirectional-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesTransferRatesInboundDevice(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/transfer-rates/inbound-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesTransferRatesOutboundDevice(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/transfer-rates/outbound-device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesTransferRatesBidirectional(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/transfer-rates/bidirectional?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesTransferRatesOutbound(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/transfer-rates/outbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesPortChannelBalanceInbound(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/port-channel/balance/inbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesPortChannelBalanceOutbound(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/port-channel/balance/outbound?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesPortChannelMemberStatus(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/port-channel/member-status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesMtu(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/mtu?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesDuplex(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/duplex?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesInconsistenciesSummary(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/inconsistencies/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesInconsistenciesDetails(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/inconsistencies/details?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesConnectivityMatrixUnmanagedNeighborsDetail(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/connectivity-matrix/unmanaged-neighbors/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesConnectivityMatrixUnmanagedNeighborsSummary(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/connectivity-matrix/unmanaged-neighbors/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesStormControlUnicast(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/storm-control/unicast?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesStormControlMulticast(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/storm-control/multicast?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInterfacesStormControlBroadcast(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/interfaces/storm-control/broadcast?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesAddressingDuplicateIp(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/addressing/duplicate-ip?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesAddressingManagedDevs(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/addressing/managed-devs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesAddressingMac(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/addressing/mac?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesAddressingArp(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/addressing/arp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesAddressingNatRules(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/addressing/nat/rules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesAddressingNatPools(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/addressing/nat/pools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesAddressingHosts(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/addressing/hosts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesVxlanPeers(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/vxlan/peers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesVxlanInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/vxlan/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesVxlanVni(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/vxlan/vni?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesVxlanVtep(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/vxlan/vtep?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesQosPolicyMaps(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/qos/policy-maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesQosPolicing(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/qos/policing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesQosShaping(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/qos/shaping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesQosPriorityQueuing(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/qos/priority-queuing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesQosMarking(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/qos/marking?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesQosQueuing(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/qos/queuing?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventoryModules(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/modules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventoryPowerSuppliesFans(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/power-supplies-fans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventoryPhones(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/phones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventoryInterfaces(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/interfaces?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventoryFans(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/fans?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventorySites(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/sites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventoryPowerSupplies(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/power-supplies?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventorySummaryVendors(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/summary/vendors?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventorySummaryPlatforms(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/summary/platforms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventorySummaryModels(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/summary/models?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventorySummaryFamilies(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/summary/families?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventoryDevices(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTablesInventoryPn(body, callback)</td>
    <td style="padding:15px"></td>
    <td style="padding:15px">{base_path}/{version}/tables/inventory/pn?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inventoryDiff(body, callback)</td>
    <td style="padding:15px">Difference in inventory after changes have been made</td>
    <td style="padding:15px">{base_path}/{version}/tables/management/changes?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deviceDiff(body, callback)</td>
    <td style="padding:15px">Difference in devices after changes have been made</td>
    <td style="padding:15px">{base_path}/{version}/tables/management/changes/devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
